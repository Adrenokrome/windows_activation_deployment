# Windows Activation Deployment

Batch script allowing Windows activation with several product keys in possession. The keys are to be inserted in the Product_key.bat file before installation.

- Change product keys in the Product_key.bat 
- Execute the script Activate_windows.bat with Administrator privilege
- Delete or not the previous key
- Enter the number of the key you want to install

![image](/uploads/ec089e2f2ac2c7b91c881588faa5e93e/image.png)