REM by Kevin Caparros - 07/07/2020 
REM for Prooftag
REM information is on : https://gitlab.com/Adrenokrome/windows-activation-deployment

chcp 65001 >nul

@echo off

CLS
ECHO.
ECHO ::::::::::::::::::::::::::::::::::::::::::::::::
ECHO :: Script Activation Windows with Product Key ::
ECHO ::::::::::::::::::::::::::::::::::::::::::::::::
ECHO.

@call "%~dp0Product_key.bat" >nul

set /p del="Voulez vous supprimer l'ancienne clé ? y or n... " 
if /I "%del%"=="y" goto delkey

ECHO.
ECHO 1- %key1%
ECHO 2- %key2%
ECHO 3- %key3%
ECHO 4- %key4%
ECHO 5- %key5% 
ECHO 6- %key6%
ECHO 7- %key7%
ECHO 8- %key8%
ECHO 9- %key9%
ECHO 10- %key10%
ECHO.

set /p choix="Entrez le numéro de la clé à installer ou tapez 'all' pour tester toutes les clés... "
if "%choix%"=="" goto fin
if "%choix%"=="1" goto active1
if "%choix%"=="2" goto active2
if "%choix%"=="3" goto active3
if "%choix%"=="4" goto active4
if "%choix%"=="5" goto active5
if "%choix%"=="6" goto active6
if "%choix%"=="7" goto active7
if "%choix%"=="8" goto active8
if "%choix%"=="9" goto active9
if "%choix%"=="10" goto active10
if "%choix%"=="all" goto activeall


:active1
cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key1% 
goto suite
 
:active2
cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key2% 
goto suite

:active3
cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key3%
goto suite

:active4 
cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key4%
goto suite

:active5 
cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key5% 
goto suite

:active6
cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key6% 
goto suite

:active7 
cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key7% 
goto suite
 
:active8
cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key8% 
goto suite
 
:active9
cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key9%
goto suite

:active10 
cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key10% 
goto suite

:activeall
cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key1%&cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key2%&cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key3%&cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key4%&cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key5%&cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key6%&cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key7%&cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key8%&cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key9%&cscript //nologo c:\windows\system32\slmgr.vbs /ipk %key10%
goto suite

:delkey
slmgr.vbs /upk


:suite
chcp 437 > nul
cscript //nologo c:\windows\system32\slmgr.vbs /ato | find /i "activ" && echo ==Successfully== || echo ==Error, take another product key==
slmgr.vbs /ato

:fin 
Pause

set choix=